<div class="d-flex align-items-center justify-content-between mb-4">
	<h4>List Barang</h4>
	<a href="<?= base_url('barang/tambah') ?>" class="btn btn-info">Tambah Barang</a>
</div>
<table class="table table-bordered">
	<thead>
		<tr>
			<th scope="col">ID</th>
			<th scope="col">Kode</th>
			<th scope="col">Nama</th>
			<th scope="col">Total Masuk</th>
			<th scope="col">Total Keluar</th>
			<th scope="col">Stock</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if (count($barangs) == 0) : ?>
			<tr>
				<th colspan="5" class="text-center">
					<h4 class="my-4">Tidak ada list barang</h4>
				</th>
			</tr>
		<?php else : ?>
			<?php
				foreach ($barangs as $barang) :
					$barangMasuk = $this->db->select('SUM(detail_pencatatan.jumlah) as jumlah')
					->from('detail_pencatatan')
					->where('id_barang', $barang->id_barang)
					->join('pencatatan', 'pencatatan.id_pencatatan = detail_pencatatan.id_pencatatan')
					->where('pencatatan.alur_pencatatan', 'masuk')
					->get()->row();

					$barangKeluar = $this->db->select('SUM(detail_pencatatan.jumlah) as jumlah')
					->from('detail_pencatatan')
					->where('id_barang', $barang->id_barang)
					->join('pencatatan', 'pencatatan.id_pencatatan = detail_pencatatan.id_pencatatan')
					->where('pencatatan.alur_pencatatan', 'keluar')
					->get()->row();
			?>
				<tr>
					<th scope="row"><?= $barang->id_barang ?></th>
					<td><?= $barang->kode ?></td>
					<td><?= $barang->nama ?></td>
					<td><?= $barangMasuk->jumlah ?? '0' ?></td>
					<td><?= $barangKeluar->jumlah ?? '0' ?></td>
					<td><?= (($barangMasuk->jumlah ?? 0) - ($barangKeluar->jumlah ?? 0)) ?></td>
					<td>
						<a href="<?= base_url('barang/detail/') . $barang->id_barang ?>" class="btn btn-sm btn-primary">Detail</a>
						<a href="<?= base_url('barang/edit/') . $barang->id_barang ?>" class="btn btn-sm btn-warning">Edit</a>
						<a href="<?= base_url('barang/hapus/') . $barang->id_barang ?>" class="btn btn-sm btn-danger">Hapus</a>
					</td>
				</tr>
		<?php endforeach;
		endif; ?>
	</tbody>
</table>
