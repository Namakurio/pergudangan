<div>
	<h4 class="mb-4">Detail Barang</h4>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Kode</label>
	<div class="col-sm-10">
		<h6><?= $barang->kode ?></h6>
	</div>
</div>

<div class="row">
	<label class="pb-1 col-sm-2">Nama</label>
	<div class="col-sm-10">
		<h6><?= $barang->nama ?></h6>
	</div>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Satuan</label>
	<div class="col-sm-10">
		<h6><?= $barang->satuan ?></h6>
	</div>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Kategori</label>
	<div class="col-sm-10">
		<h6><?= $barang->kategori ?></h6>
	</div>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Keterangan</label>
	<div class="col-sm-10">
		<h6><?= $barang->keterangan ?></h6>
	</div>
</div>

<a href="<?= base_url('barang/edit/') . $barang->id_barang ?>" class="btn btn-info mt-2">Edit Barang</a>
<a href="<?= base_url('barang') ?>" class="btn btn-secondary mt-2">Kembali</a>
