<div>
	<h4 class="mb-4">Edit Barang</h4>
</div>
<form method="post" action="<?= base_url('barang/update/') . $barang->id_barang ?>" class="mb-4">
	<div class="form-group row">
		<label for="input-code" class="col-sm-2 col-form-label">Kode</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="input-code" name="kode" value="<?= $barang->kode ?>" required>
		</div>
	</div>

	<div class="form-group row">
		<label for="input-nama" class="col-sm-2 col-form-label">Nama</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="input-nama" name="nama" value="<?= $barang->nama ?>" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="input-satuan" class="col-sm-2 col-form-label">Satuan</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="input-satuan" name="satuan" value="<?= $barang->satuan ?>" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="input-kategori" class="col-sm-2 col-form-label">Kategori</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="input-kategori" name="kategori" value="<?= $barang->kategori ?>" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="input-keterangan" class="col-sm-2 col-form-label">Keterangan</label>
		<div class="col-sm-10">
			<textarea id="input-keterangan" class="form-control" cols="30" rows="5" name="keterangan"><?= $barang->keterangan ?></textarea>
		</div>
	</div>

	<button type="submit" class="btn btn-info">Simpan</button>
	<a href="<?= base_url('barang') ?>" class="btn btn-secondary">Batal</a>
</form>
