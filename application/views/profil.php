<div class="col-sm-12">
	<form method="post" action="<?= base_url('profil/update/akun') ?>">
		<h4>Edit Profil</h4>
		<div class="row">
			<div class="form-group col-md-6 col-12">
				<label>Username</label>
				<input type="text" class="form-control" value="<?= $this->session->userdata('username') ?>" required="" name="username">
			</div>
			<div class="form-group col-md-6 col-12">
				<label>Nama</label>
				<input type="text" class="form-control" value="<?= $this->session->userdata('nama') ?>" required="" name="nama">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-sm-12">
				<label>Telepon</label>
				<input type="phone" class="form-control" value="<?= $this->session->userdata('telepon') ?>" required="" name="telepon">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-6 col-12">
				<label>Jenis Kelamin</label>
				<select name="jenis_kelamin" class="form-control">
					<option value="L" <?= $this->session->userdata('jenis_kelamin') == 'L' ? 'selected': '' ?>>Laki - laki</option>
					<option value="P" <?= $this->session->userdata('jenis_kelamin') == 'P' ? 'selected': '' ?>>Perempuan</option>
				</select>
			</div>
			<div class="form-group col-md-6 col-12">
				<label>Tanggal Lahir</label>
				<input type="date" class="form-control" value="<?= $this->session->userdata('tanggal_lahir') ?>" required="" name="tanggal_lahir">
			</div>
		</div>
		<button class="btn btn-info">Simpan Perubahan</button>
	</form>
</div>
