</main>

<div class="modal fade" id="ubah-password" tabindex="-1" aria-labelledby="ubah-password-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="ubah-password-label">Ubah Password</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" action="<?= base_url('profil/update/password') ?>">
				<div class="modal-body">
					<div class="form-group">
						<label for="password-now" class="col-form-label">Password Saat Ini</label>
						<input type="password" class="form-control" id="password-now" name="password-now" required>
					</div>
					<div class="form-group">
						<label for="password-new" class="col-form-label">Password Baru</label>
						<input type="password" class="form-control" id="password-new" name="password-new" required>
					</div>
					<div class="form-group">
						<label for="reply-password-new" class="col-form-label">Ulang Password Baru</label>
						<input type="password" class="form-control" id="reply-password-new" name="reply-password-new" required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-info">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
<script>
	function tambahBarangPencatatan(element) {
		html = `
		<li class="list-group-item lh-condensed">
			<div class="form-group row">
				<label for="input-barang" class="col-sm-2 col-form-label">Nama Barang</label>
				<div class="col-sm-10">
					<select name="barang[]" id="input-barang" class="form-control">
						<?php if (isset($barangs)) : ?>
							<?php foreach ($barangs as $barang) : ?>
								<option value="<?= $barang->id_barang ?>"><?= $barang->nama ?></option>
							<?php endforeach;
						endif; ?>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="input-jumlah" class="col-sm-2 col-form-label">Jumlah</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="input-jumlah" name="jumlah[]">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-10">
					<a href="javascript:void(0);" class="btn btn-danger" onclick="hapusBarangPencatatan(this)">Hapus</a>
				</div>
			</div>
		</li>`

		$(element).append(html)
	}

	function hapusBarangPencatatan(element) {
		$(element).parent().parent().parent().remove();
	}

	function print(element) {
		$("#" + element).show();
		printJS(element, 'html');
		$("#" + element).hide();
	}
</script>
</body>

</html>
