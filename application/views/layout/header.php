<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Aplikasi Pergudangan</title>

	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
</head>

<body>
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-info fixed-top">
		<div class="container">
			<a class="navbar-brand" href="<?= base_url() ?>">Pergudangan</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item  <?= $this->uri->segment(1) == '' ? 'active' : '' ?>">
						<a class="nav-link" href="<?= base_url() ?>">Home</a>
					</li>
					<li class="nav-item <?= $this->uri->segment(1) == 'barang' ? 'active' : '' ?>">
						<a class="nav-link" href="<?= base_url('barang') ?>">Barang</a>
					</li>
					<li class="nav-item dropdown <?= $this->uri->segment(1) == 'pencatatan' ? (isset($alur) && $alur == 'masuk' ? 'active' : '') : '' ?>">
						<a class="nav-link dropdown-toggle" href="#" id="navbar-pencatatan-dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Stock Masuk
						</a>
						<div class="dropdown-menu" aria-labelledby="navbar-pencatatan-dropdown">
							<a class="dropdown-item" href="<?= base_url('pencatatan?alur=masuk') ?>">Data Stock Masuk</a>
							<a class="dropdown-item" href="<?= base_url('pencatatan/tambah?alur=masuk') ?>">Input Barang Masuk</a>
						</div>
					</li>
					<li class="nav-item dropdown <?= $this->uri->segment(1) == 'pencatatan' ? (isset($alur) && $alur == 'keluar' ? 'active' : '') : '' ?>">
						<a class="nav-link dropdown-toggle" href="#" id="navbar-pencatatan-dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Stock Keluar
						</a>
						<div class="dropdown-menu" aria-labelledby="navbar-pencatatan-dropdown">
							<a class="dropdown-item" href="<?= base_url('pencatatan?alur=keluar') ?>">Data Stock Keluar</a>
							<a class="dropdown-item" href="<?= base_url('pencatatan/tambah?alur=keluar') ?>">Input Barang Keluar</a>
						</div>
					</li>
					<li class="nav-item  <?= $this->uri->segment(1) == 'user' ? 'active' : '' ?>">
						<a class="nav-link" href="<?= base_url('User') ?>">Admin</a>
					</li>
				</ul>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbar-admin-dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?= $this->session->userdata('nama') ?>
						</a>
						<div class="dropdown-menu" aria-labelledby="navbar-admin-dropdown">
							<a class="dropdown-item" href="<?= base_url('profil') ?>">Profil</a>
							<a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#ubah-password">Ubah Password</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="<?= base_url('logout') ?>">Keluar</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<main role="main" class="container">
		<?php if ($this->session->flashdata('alert')) { ?>
			<div class="alert alert-<?= $this->session->flashdata('alert')['type'] ?> alert-dismissible text-left" role="alert">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?= $this->session->flashdata('alert')['message'] ?>
			</div>
		<?php } ?>
		<?= validation_errors() ?>
