<div>
	<h4 class="mb-4">Detail Admin</h4>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Username</label>
	<div class="col-sm-10">
		<h6><?= $user->username ?></h6>
	</div>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Nama</label>
	<div class="col-sm-10">
		<h6><?= $user->nama ?></h6>
	</div>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Telepon</label>
	<div class="col-sm-10">
		<h6><?= $user->telepon ?></h6>
	</div>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Jenis Kelamin</label>
	<div class="col-sm-10">
		<h6><?= $user->jenis_kelamin == 'L' ? 'Laki - laki' : 'Perempuan' ?></h6>
	</div>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Tanggal Lahir</label>
	<div class="col-sm-10">
		<h6><?= $user->tanggal_lahir ?></h6>
	</div>
</div>

<a href="<?= base_url('user/edit/') . $user->id_admin ?>" class="btn btn-info mt-2">Edit Admin</a>
<a href="<?= base_url('user') ?>" class="btn btn-secondary mt-2">Kembali</a>
