<div class="d-flex align-items-center justify-content-between mb-4">
	<h4>List Admin</h4>
	<a href="<?= base_url('user/tambah') ?>" class="btn btn-info">Tambah Admin</a>
</div>
<table class="table table-bordered">
	<thead>
		<tr>
			<th scope="col">ID</th>
			<th scope="col">Username</th>
			<th scope="col">Nama</th>
			<th scope="col">Telepon</th>
			<th scope="col">Jenis Kelamin</th>
			<th scope="col">Tanggal Lahir</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if (count($users) == 0) : ?>
			<tr>
				<th colspan="5" class="text-center">
					<h4 class="my-4">Tidak ada list admin</h4>
				</th>
			</tr>
		<?php else : ?>
			<?php foreach ($users as $user) : ?>
				<tr>
					<th scope="row"><?= $user->id_admin ?></th>
					<td><?= $user->username ?></td>
					<td><?= $user->nama ?></td>
					<td><?= $user->telepon ?></td>
					<td><?= $user->jenis_kelamin ?></td>
					<td><?= $user->tanggal_lahir ?></td>
					<td>
						<?php if ($this->session->userdata('id_admin') != $user->id_admin) : ?>
							<a href="<?= base_url('user/detail/') . $user->id_admin ?>" class="btn btn-sm btn-primary">Detail</a>
							<a href="<?= base_url('user/edit/') . $user->id_admin ?>" class="btn btn-sm btn-warning">Edit</a>
							<a href="<?= base_url('user/hapus/') . $user->id_admin ?>" class="btn btn-sm btn-danger">Hapus</a>
						<?php endif; ?>
					</td>
				</tr>
		<?php endforeach;
		endif; ?>
	</tbody>
</table>
