<div>
	<h4 class="mb-4">Edit Admin</h4>
</div>
<form method="post" action="<?= base_url('user/update/') . $user->id_admin ?>" class="mb-4">
	<div class="form-group row">
		<label for="input-username" class="col-sm-2 col-form-label">Username</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="input-username" name="username" value="<?= $user->username ?>" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="input-password" class="col-sm-2 col-form-label">Password</label>
		<div class="col-sm-10">
			<input type="password" class="form-control" id="input-password" name="password">
			<small class="form-text text-muted">Kosongkan Password jika tidak ingin mengubahnya</small>
		</div>
	</div>
	<div class="form-group row">
		<label for="input-nama" class="col-sm-2 col-form-label">Nama</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="input-nama" name="nama" value="<?= $user->nama ?>" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="input-telepon" class="col-sm-2 col-form-label">Telepon</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="input-telepon" name="telepon" value="<?= $user->telepon ?>" required>
		</div>
	</div>
	<div class="form-group row">
		<label for="input-jenis-kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
		<div class="col-sm-10">
			<select name="jenis_kelamin" id="input-jenis-kelamin" class="form-control" required>
				<option value="L" <?= $user->jenis_kelamin == 'L' ? 'selected' : '' ?>>Laki -laki</option>
				<option value="P" <?= $user->jenis_kelamin == 'P' ? 'selected' : '' ?>>Perempuan</option>
			</select>
		</div>
	</div>
	<div class="form-group row">
		<label for="input-tanggal-lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
		<div class="col-sm-10">
			<input type="date" class="form-control" id="input-tanggal-lahir" name="tanggal_lahir" value="<?= $user->tanggal_lahir ?>" required>
		</div>
	</div>

	<button type="submit" class="btn btn-info">Simpan</button>
	<a href="<?= base_url('user') ?>" class="btn btn-secondary">Batal</a>
</form>
