<div>
	<h4 class="mb-4">Edit Barang <?= ucfirst($alur) ?></h4>
</div>
<form method="post" action="<?= base_url('pencatatan/update/') . $pencatatan->id_pencatatan . '?alur=' . $alur ?>" class="mb-4">
	<div class="form-group row">
		<label for="input-tanggal" class="col-sm-2 col-form-label">Tanggal</label>
		<div class="col-sm-10">
			<input type="text" value="<?= $pencatatan->tanggal ?>" class="form-control" id="input-tanggal" readonly>
		</div>
	</div>
	<div class="form-group row">
		<label for="input-alur" class="col-sm-2 col-form-label">Alur</label>
		<div class="col-sm-10">
			<input type="text" value="<?= ucfirst($pencatatan->alur_pencatatan) ?>" class="form-control" id="input-alur" name="alur" readonly>
			<!-- <select name="alur" id="input-alur" class="form-control">
				<option value="masuk" <?= $pencatatan->alur_pencatatan == 'masuk' ? 'selected' : '' ?>>Masuk</option>
				<option value="keluar" <?= $pencatatan->alur_pencatatan == 'keluar' ? 'selected' : '' ?>>Keluar</option>
			</select> -->
		</div>
	</div>
	<div class="form-group row">
		<label for="input-keterangan" class="col-sm-2 col-form-label">Keterangan</label>
		<div class="col-sm-10">
			<textarea name="keterangan" id="input-keterangan" class="form-control" cols="30" rows="5"><?= $pencatatan->keterangan ?></textarea>
		</div>
	</div>
	<div class="d-flex align-items-center justify-content-between mt-4 mb-3">
		<h5>Barang</h5>
		<a href="javascript:void(0);" class="btn btn-info" onclick="tambahBarangPencatatan('#barang-list')">Tambah Barang</a>
	</div>
	<ul class="list-group mb-3" id="barang-list">
		<?php foreach ($details as $item) : ?>
			<li class="list-group-item lh-condensed">
				<div class="form-group row">
					<label for="input-barang" class="col-sm-2 col-form-label">Nama Barang</label>
					<div class="col-sm-10">
						<select name="barang[]" id="input-barang" class="form-control">
							<?php foreach ($barangs as $barang) : ?>
								<option value="<?= $barang->id_barang ?>" <?= $barang->id_barang == $item->id_barang ? 'selected' : '' ?>><?= $barang->nama ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="input-jumlah" class="col-sm-2 col-form-label">Jumlah</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="input-jumlah" name="jumlah[]" value="<?= $item->jumlah ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-10">
						<a href="javascript:void(0);" class="btn btn-danger" onclick="hapusBarangPencatatan(this)">Hapus</a>
					</div>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>

	<button type="submit" class="btn btn-info">Simpan</button>
	<a href="<?= base_url('pencatatan?alur=' . $alur) ?>" class="btn btn-secondary">Batal</a>
</form>
