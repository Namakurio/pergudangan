<div class="d-flex align-items-center justify-content-between mb-4">
	<h4>Data Stock <?= ucfirst($alur) ?></h4>
	<div>
		<a href="<?= base_url('pencatatan/tambah?alur=' . $alur) ?>" class="btn btn-info">Input Barang <?= ucfirst($alur) ?></a>
		<a href="javascript:void(0);" class="btn btn-primary" onclick="print('printable-list-pencatatan');">Cetak</a>
	</div>
</div>
<table class="table table-bordered">
	<thead>
		<tr>
			<th scope="col">ID</th>
			<th scope="col">Tanggal</th>
			<th scope="col">Alur</th>
			<th scope="col">Keterangan</th>
			<th scope="col">Total Barang</th>
			<th scope="col">List Barang</th>
			<th scope="col">Ditambahkan</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if (count($pencatatans) == 0) : ?>
			<tr>
				<th colspan="5" class="text-center">
					<h4 class="my-4">Tidak ada list Pencatatan</h4>
				</th>
			</tr>
		<?php else : ?>
			<?php foreach ($pencatatans as $pencatatan) : ?>
				<tr>
					<th scope="row"><?= $pencatatan->id_pencatatan ?></th>
					<td><?= $pencatatan->tanggal ?></td>
					<td><?= ucfirst($pencatatan->alur_pencatatan) ?></td>
					<td><?= $pencatatan->keterangan ?></td>
					<td><?= $pencatatan->count_detail_pencatatan ?></td>
					<td>
						<?php foreach ($pencatatan->detail_pencatatans as $key => $detail_pencatatan) : ?>
							<?= "{$detail_pencatatan->nama} : {$detail_pencatatan->jumlah} {$detail_pencatatan->satuan}" ?><?= ($key == (count($pencatatan->detail_pencatatans) - 1)) ? '' : ', ' ?>
						<?php endforeach; ?>
					</td>
					<td><?= $pencatatan->admin_nama ?></td>
					<td>
						<a href="<?= base_url('pencatatan/detail/') . $pencatatan->id_pencatatan . '?alur=' . $alur ?>" class="btn btn-sm btn-primary">Detail</a>
						<a href="<?= base_url('pencatatan/edit/') . $pencatatan->id_pencatatan . '?alur=' . $alur ?>" class="btn btn-sm btn-warning">Edit</a>
						<a href="<?= base_url('pencatatan/hapus/') . $pencatatan->id_pencatatan . '?alur=' . $alur ?>" class="btn btn-sm btn-danger">Hapus</a>
					</td>
				</tr>
		<?php endforeach;
		endif; ?>
	</tbody>
</table>

<div id="printable-list-pencatatan" style="display:none;">
	<style>
		@media print {
			body,
			div,
			table,
			thead,
			tbody,
			tfoot,
			tr,
			th,
			td,
			p {
				font-family: "Calibri";
			}

			table {
				border-collapse: collapse;
			}
			table thead th {
				border:"1px solid #dee2e6";
				border-bottom:"2px solid #dee2e6";
			}
			table th {
				border:"1px solid #dee2e6";
			}
			table th {
				border:"1px solid #dee2e6";
			}
		}
	</style>
	<table border="0" style="width:100%;">
		<tr>
			<td height="21" align="center" valign=middle><b><font size=6 color="#000000"><?= COMPANY_NAME ?></font></b></td>
		</tr>
		<tr>
			<td height="21" align="center" valign=middle><b><font size=3 color="#000000"><?= COMPANY_ADDRESS ?></font></b></td>
		</tr>
		<tr>
			<td height="21" align="center" valign=middle><b><font size=4 color="#000000"><hr></font></b></td>
		</tr>
		<tr>
			<td height="21" align="center" valign=middle><b><font size=4 color="#000000">Laporan Stock <?= ucfirst($alur) ?></font></b></td>
		</tr>
		<tr>
			<td height="21" align="center" valign=middle><b><font size=3 color="#000000"><?= date('d F Y') ?></font></b></td>
		</tr>
		<tr>
			<td height="21" align="center" valign=middle><b><font size=4 color="#000000"></font></b></td>
		</tr>
	</table>
	<table style="border:1px solid #dee2e6;width:100%;">
		<thead>
			<tr>
				<th style="border:1px solid #dee2e6;border-bottom:2px solid #dee2e6;padding:7px;">ID</th>
				<th style="border:1px solid #dee2e6;border-bottom:2px solid #dee2e6;padding:7px;">Tanggal</th>
				<th style="border:1px solid #dee2e6;border-bottom:2px solid #dee2e6;padding:7px;">Alur</th>
				<th style="border:1px solid #dee2e6;border-bottom:2px solid #dee2e6;padding:7px;">Keterangan</th>
				<th style="border:1px solid #dee2e6;border-bottom:2px solid #dee2e6;padding:7px;">Total Barang</th>
				<th style="border:1px solid #dee2e6;border-bottom:2px solid #dee2e6;padding:7px;">List Barang</th>
				<th style="border:1px solid #dee2e6;border-bottom:2px solid #dee2e6;padding:7px;">Ditambahkan</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($pencatatans) == 0) : ?>
				<tr>
					<th colspan="5">
						<h4>Tidak ada list Pencatatan</h4>
					</th>
				</tr>
			<?php else : ?>
				<?php foreach ($pencatatans as $pencatatan) : ?>
					<tr>
						<th style="border:1px solid #dee2e6;padding:7px;"><?= $pencatatan->id_pencatatan ?></th>
						<td style="border:1px solid #dee2e6;padding:7px;"><?= $pencatatan->tanggal ?></td>
						<td style="border:1px solid #dee2e6;padding:7px;"><?= ucfirst($pencatatan->alur_pencatatan) ?></td>
						<td style="border:1px solid #dee2e6;padding:7px;"><?= $pencatatan->keterangan ?></td>
						<td style="border:1px solid #dee2e6;padding:7px;"><?= $pencatatan->count_detail_pencatatan ?></td>
						<td style="border:1px solid #dee2e6;padding:7px;">
							<?php foreach ($pencatatan->detail_pencatatans as $key => $detail_pencatatan) : ?>
								<?= "{$detail_pencatatan->nama} : {$detail_pencatatan->jumlah} {$detail_pencatatan->satuan}" ?><?= ($key == (count($pencatatan->detail_pencatatans) - 1)) ? '' : ', ' ?>
							<?php endforeach; ?>
						</td>
						<td style="border:1px solid #dee2e6;padding:7px;"><?= $pencatatan->admin_nama ?></td>
					</tr>
			<?php endforeach;
			endif; ?>
		</tbody>
	</table>

	<table border="0" style="width:100%;margin-top:40px;">
            <colgroup width="131"></colgroup>
            <colgroup width="12"></colgroup>
            <colgroup width="248"></colgroup>
            <colgroup width="103"></colgroup>
            <colgroup width="10"></colgroup>
            <colgroup width="113"></colgroup>
            <tr>
                <td height="21" align="center" valign=middle><b><font size=3 color="#000000">TTD</font></b></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=top><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
            </tr>
            <tr>
                <td height="21" align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=top><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
            </tr>
            <tr>
                <td height="21" align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=top><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
            </tr>
            <tr>
                <td height="21" align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=top><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
            </tr>

            <tr>
                <td height="21" align="center" valign=middle><font size=3 color="#000000"><?= $this->session->userdata('nama') ?></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=top><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
                <td align="left" valign=bottom><font size=3 color="#000000"><br></font></td>
            </tr>
        </table>
</div>
