<div>
	<h4 class="mb-4">Detail Barang <?= ucfirst($alur) ?></h4>
</div>

<div class="row">
	<label class="pb-1 col-sm-2">Tanggal</label>
	<div class="col-sm-10">
		<h6><?= $pencatatan->tanggal ?></h6>
	</div>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Alur</label>
	<div class="col-sm-10">
		<h6><?= ucfirst($pencatatan->alur_pencatatan) ?></h6>
	</div>
</div>
<div class="row">
	<label class="pb-1 col-sm-2">Keterangan</label>
	<div class="col-sm-10">
		<h6><?= $pencatatan->keterangan ?></h6>
	</div>
</div>
<div class="d-flex align-items-center justify-content-between mt-4 mb-3">
	<h5>Barang</h5>
</div>
<ul class="list-group mb-3">
	<?php if (count($details) == 0) : ?>
		<li class="list-group-item lh-condensed">
			<div class="row">
				<div class="col-sm-12">
					<h4 class="text-center">Tidak ada barang</h4>
				</div>
			</div>
		</li>
	<?php else : ?>
		<?php foreach ($details as $item) : ?>
			<li class="list-group-item lh-condensed">
				<div class="row">
					<label class="col-sm-2">Nama</label>
					<div class="col-sm-10">
						<h6><?= $item->nama ?></h6>
					</div>
				</div>
				<div class="row">
					<label class="col-sm-2">Jumlah</label>
					<div class="col-sm-10">
						<h6><?= $item->jumlah ?></h6>
					</div>
				</div>
			</li>
	<?php endforeach;
	endif; ?>
</ul>

<a href="<?= base_url('pencatatan/edit/') . $pencatatan->id_pencatatan . '?alur=' . $alur ?>" class="btn btn-info">Edit Stock</a>
<a href="<?= base_url('pencatatan?alur=' . $alur) ?>" class="btn btn-secondary">Kembali</a>
