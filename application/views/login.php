<!doctype html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login - Pergudangan</title>

	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/login.css') ?>">
</head>

<body class="text-center">
	<form class="form-login" action="<?= base_url('login/proses') ?>" method="POST">
		<h1 class="h3 mb-4 font-weight-normal">Login</h1>
		<?php if ($this->session->flashdata('alert')) { ?>
			<div class="alert alert-<?= $this->session->flashdata('alert')['type'] ?> alert-dismissible text-left" role="alert">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?= $this->session->flashdata('alert')['message'] ?>
			</div>
		<?php } ?>
		<label for="inputEmail" class="sr-only">Username</label>
		<input type="username" name="username" class="form-control" placeholder="Username" required autofocus>
		<label for="inputPassword" class="sr-only">Password</label>
		<input type="password" name="password" class="form-control" placeholder="Password" required>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
		<p class="mt-5 mb-3 text-muted">&copy; <?= date('Y') ?></p>
	</form>

	<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>
