<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pencatatan_model extends CI_Model
{
	protected $table = 'pencatatan';

	public function __construct()
	{
		parent::__construct();
	}

	public function list($alur = null)
	{
		$query = $this->db->select('pencatatan.*, admin.nama as admin_nama, COUNT(detail_pencatatan.jumlah) as count_detail_pencatatan')->from($this->table)
			->join('admin', 'admin.id_admin = pencatatan.id_admin')
			->join('detail_pencatatan', 'detail_pencatatan.id_pencatatan = pencatatan.id_pencatatan')
			->group_by('pencatatan.id_pencatatan');

		if ($alur) {
			$query->where('pencatatan.alur_pencatatan', $alur);
		}

		return $query->get()->result();
	}

	public function get_by_id($id)
	{
		return $this->db->get_where($this->table, ['id_pencatatan' => $id])->row();
	}

	public function insert($id_admin)
	{
		$data = [
			'tanggal' => date('Y-m-d'),
			'alur_pencatatan' => strtolower($_POST['alur']),
			'keterangan' => $_POST['keterangan'],
			'id_admin' => $id_admin
		];

		return $this->db->insert($this->table, $data);
	}

	public function update($id)
	{
		$data = [
			'alur_pencatatan' => strtolower($_POST['alur']),
			'keterangan' => $_POST['keterangan'],
		];

		$where = ['id_pencatatan' => $id];
		return $this->db->update($this->table, $data, $where);
	}

	public function delete($id)
	{
		$this->db->delete($this->table, ['id_pencatatan' => $id]);

		return true;
	}

	public function detail_list($id)
	{
		return $this->db->where('detail_pencatatan.id_pencatatan', $id)
			->join('barang', 'barang.id_barang = detail_pencatatan.id_barang')
			->get('detail_pencatatan')->result();
	}

	public function changeDetailPencatatan($id, $data)
	{
		$this->db->delete('detail_pencatatan', ['id_pencatatan' => $id]);

		foreach ($data as $key => $item) {
			$this->db->insert('detail_pencatatan', $item);
		}

		return true;
	}
}
