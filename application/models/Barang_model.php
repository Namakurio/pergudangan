<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Barang_model extends CI_Model
{
	protected $table = 'barang';

	public function __construct()
	{
		parent::__construct();
	}

	public function list()
	{
		return $this->db->get($this->table)->result();
	}

	public function get_by_id($id)
	{
		return $this->db->get_where($this->table, ['id_barang' => $id])->row();
	}

	public function insert()
	{
		$data = [
			'kode' => $_POST['kode'],
			'nama' => $_POST['nama'],
			'satuan' => $_POST['satuan'],
			'kategori' => $_POST['kategori'],
			'keterangan' => $_POST['keterangan']
		];

		return $this->db->insert($this->table, $data);
	}

	public function update($id)
	{
		$data = [
			'kode' => $_POST['kode'],
			'nama' => $_POST['nama'],
			'satuan' => $_POST['satuan'],
			'kategori' => $_POST['kategori'],
			'keterangan' => $_POST['keterangan']
		];

		$where = ['id_barang' => $id];
		return $this->db->update($this->table, $data, $where);
	}

	public function delete($id)
	{
		$this->db->delete($this->table, array('id_barang' => $id));

		return true;
	}
}
