<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
	protected $table = 'admin';

	public function __construct()
	{
		parent::__construct();
	}

	public function check_username($username)
	{
		return $this->db->select('username')
			->from($this->table)
			->where('username', $username)
			->get();
	}

	public function login($username, $password)
	{
		$query = $this->db->select('*')->from($this->table)->where('username', $username)->where('password', $password);

		return $query->get();
	}

	public function check_password($id_admin, $password)
	{
		return $this->db->select('*')
			->from($this->table)
			->where('id_admin', $id_admin)
			->where('password', $password)
			->get();
	}

	public function change_password($id_admin, $password)
	{
		$data = [
			'password' => $password,
		];

		$where = ['id_admin' => $id_admin];
		return $this->db->update($this->table, $data, $where);
	}

	public function update_profil($id_admin)
	{
		$data = [
			'username' => $_POST['username'],
			'nama' => $_POST['nama'],
			'telepon' => $_POST['telepon'],
			'jenis_kelamin' => $_POST['jenis_kelamin'],
			'tanggal_lahir' => $_POST['tanggal_lahir'],
		];

		$where = ['id_admin' => $id_admin];
		return $this->db->update($this->table, $data, $where);
	}

	public function list()
	{
		return $this->db->get($this->table)->result();
	}

	public function get_by_id($id)
	{
		return $this->db->get_where($this->table, ['id_admin' => $id])->row();
	}

	public function insert()
	{
		$data = [
			'username' => $_POST['username'],
			'password' => $_POST['password'],
			'telepon' => $_POST['telepon'],
			'jenis_kelamin' => $_POST['jenis_kelamin'],
			'nama' => $_POST['nama'],
			'tanggal_lahir' => $_POST['tanggal_lahir'],
		];

		return $this->db->insert($this->table, $data);
	}

	public function update($id)
	{
		$data = [
			'username' => $_POST['username'],
			'telepon' => $_POST['telepon'],
			'jenis_kelamin' => $_POST['jenis_kelamin'],
			'nama' => $_POST['nama'],
			'tanggal_lahir' => $_POST['tanggal_lahir'],
		];

		if($_POST['password'] != null || $_POST['password'] != '') {
			$data['password'] = $_POST['password'];
		}

		$where = ['id_admin' => $id];
		return $this->db->update($this->table, $data, $where);
	}

	public function delete($id)
	{
		$this->db->delete($this->table, array('id_admin' => $id));

		return true;
	}
}
