<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model', 'admin_model');

		if (!$this->session->userdata('login')) {
			redirect(base_url());
		}
	}

	public function index()
	{
		$users = $this->admin_model->list();

		$data['users'] = $users;

		$this->load->view('layout/header');
		$this->load->view('user/index', $data);
		$this->load->view('layout/footer');
	}

	public function tambah()
	{
		$this->load->view('layout/header');
		$this->load->view('user/tambah');
		$this->load->view('layout/footer');
	}

	public function save()
	{
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');

		if ($this->form_validation->run() == TRUE) {
			$username = $this->input->post('username');

			$check_username = $this->admin_model->check_username($username);

			if ($check_username->num_rows() > 0) {
				$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Username sudah terdaftar']);

				return redirect(base_url('user/tambah'));
			} else {
				$insert =  $this->admin_model->insert();

				if ($insert) {
					$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil menambahkan user']);

					return redirect(base_url('user/detail/') . $this->db->insert_id());
				} else {
					$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Gagal menambahkan user']);

					return redirect(base_url('user'));
				}
			}
		}
	}

	public function detail($id)
	{
		$user = $this->admin_model->get_by_id($id);
		$data['user'] = $user;

		$this->load->view('layout/header');
		$this->load->view('user/detail', $data);
		$this->load->view('layout/footer');
	}

	public function edit($id)
	{
		$user = $this->admin_model->get_by_id($id);
		$data['user'] = $user;

		$this->load->view('layout/header');
		$this->load->view('user/edit', $data);
		$this->load->view('layout/footer');
	}

	public function update($id)
	{
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');

		if ($this->form_validation->run() == TRUE) {
			$update =  $this->admin_model->update($id);

			if ($update) {
				$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil memperbarui user']);
			} else {
				$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Gagal memperbarui user']);
			}
		}
		return redirect(base_url('user/detail/') . $id);
	}

	public function hapus($id)
	{
		$hapus = $this->admin_model->delete($id);

		$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil menghapus user']);

		return redirect(base_url('user'));
	}
}
