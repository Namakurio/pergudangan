<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Barang_model', 'barang_model');

		if (!$this->session->userdata('login')) {
			redirect(base_url());
		}
	}

	public function index()
	{
		$barangs = $this->barang_model->list();

		$data['barangs'] = $barangs;

		$this->load->view('layout/header');
		$this->load->view('barang/index', $data);
		$this->load->view('layout/footer');
	}

	public function tambah()
	{
		$this->load->view('layout/header');
		$this->load->view('barang/tambah');
		$this->load->view('layout/footer');
	}

	public function save()
	{
		$this->form_validation->set_rules('kode', 'Kode', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('satuan', 'Satuan', 'required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if ($this->form_validation->run() == TRUE) {
			$insert =  $this->barang_model->insert();

			if($insert) {
				$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil menambahkan barang']);

				return redirect(base_url('barang/detail/') . $this->db->insert_id());
			} else {
				$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Gagal menambahkan barang']);

				return redirect(base_url('barang'));
			}
		}
	}

	public function detail($id)
	{
		$barang = $this->barang_model->get_by_id($id);
		$data['barang'] = $barang;

		$this->load->view('layout/header');
		$this->load->view('barang/detail', $data);
		$this->load->view('layout/footer');
	}

	public function edit($id)
	{
		$barang = $this->barang_model->get_by_id($id);
		$data['barang'] = $barang;

		$this->load->view('layout/header');
		$this->load->view('barang/edit', $data);
		$this->load->view('layout/footer');
	}

	public function update($id)
	{
		$this->form_validation->set_rules('kode', 'Kode', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('satuan', 'Satuan', 'required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if ($this->form_validation->run() == TRUE) {
			$update =  $this->barang_model->update($id);

			if($update) {
				$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil memperbarui barang']);
			} else {
				$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Gagal memperbarui barang']);
			}
		}
		return redirect(base_url('barang/detail/') . $id);
	}

	public function hapus($id)
	{
		$hapus = $this->barang_model->delete($id);

		$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil menghapus barang']);

		return redirect(base_url('barang'));
	}
}
