<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pencatatan extends CI_Controller
{
	protected $query = null;
	protected $alur = 'masuk';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pencatatan_model', 'pencatatan_model');
		$this->load->model('Barang_model', 'barang_model');

		if (!$this->session->userdata('login')) {
			redirect(base_url());
		}

		parse_str($_SERVER['QUERY_STRING'], $this->query);

		if (isset($this->query['alur'])) {
			if ($this->query['alur'] == 'masuk' || $this->query['alur'] == 'keluar') {
				$this->alur = $this->query['alur'];
			}
		}
	}

	public function index()
	{
		$pencatatans = $this->pencatatan_model->list($this->alur);
		foreach($pencatatans as $key => $pencatatan) {
			$detail_pencatatans = $this->pencatatan_model->detail_list($pencatatan->id_pencatatan);
			$pencatatans[$key]->detail_pencatatans = $detail_pencatatans;
		}

		$data['pencatatans'] = $pencatatans;
		$data['alur'] = $this->alur;

		$this->load->view('layout/header', $data);
		$this->load->view('pencatatan/index', $data);
		$this->load->view('layout/footer');
	}

	public function tambah()
	{
		$barangs = $this->barang_model->list();

		$data['barangs'] = $barangs;
		$data['alur'] = $this->alur;

		$this->load->view('layout/header', $data);
		$this->load->view('pencatatan/tambah', $data);
		$this->load->view('layout/footer');
	}

	public function save()
	{
		$this->form_validation->set_rules('alur', 'Alur', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if ($this->form_validation->run() == TRUE) {
			$id_admin = $this->session->userdata('id_admin');
			$insert =  $this->pencatatan_model->insert($id_admin);

			$id_pencatatan = $this->db->insert_id();
			if ($insert) {
				$this->saveDetailPencatatan($id_pencatatan);

				$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil input barang ' . $this->alur]);

				return redirect(base_url('pencatatan/detail/') . $id_pencatatan . '?alur=' . $this->alur);
			} else {
				$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Gagal input barang ' . $this->alur]);

				return redirect(base_url('pencatatan?alur=' . $this->alur));
			}
		}
	}

	public function detail($id)
	{
		$pencatatan = $this->pencatatan_model->get_by_id($id);
		$details = $this->pencatatan_model->detail_list($id);
		$data['pencatatan'] = $pencatatan;
		$data['details'] = $details;
		$data['alur'] = $this->alur;

		$this->load->view('layout/header', $data);
		$this->load->view('pencatatan/detail', $data);
		$this->load->view('layout/footer');
	}

	public function edit($id)
	{
		$barangs = $this->barang_model->list();
		$pencatatan = $this->pencatatan_model->get_by_id($id);
		$details = $this->pencatatan_model->detail_list($id);

		$data['pencatatan'] = $pencatatan;
		$data['barangs'] = $barangs;
		$data['details'] = $details;
		$data['alur'] = $this->alur;

		$this->load->view('layout/header', $data);
		$this->load->view('pencatatan/edit', $data);
		$this->load->view('layout/footer');
	}

	public function update($id)
	{
		$this->form_validation->set_rules('alur', 'Alur', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if ($this->form_validation->run() == TRUE) {
			$update =  $this->pencatatan_model->update($id);

			if ($update) {
				$this->saveDetailPencatatan($id);
				$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil memperbarui barang ' . $this->alur]);
			} else {
				$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Gagal memperbarui barang ' . $this->alur]);
			}
		}
		return redirect(base_url('pencatatan/detail/') . $id . '?alur=' . $this->alur);
	}

	public function hapus($id)
	{
		$hapus = $this->pencatatan_model->delete($id);

		$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil menghapus barang ' . $this->alur]);

		return redirect(base_url('pencatatan?alur=' . $this->alur));
	}

	protected function saveDetailPencatatan($id_pencatatan)
	{
		$barangIds = $_POST['barang'] ?? null;
		$jumlahs = $_POST['jumlah'] ?? null;
		$data = [];

		if ($barangIds) {
			foreach ($barangIds as $key => $id_barang) {
				$data[] = [
					'id_pencatatan' => $id_pencatatan,
					'id_barang' => $id_barang,
					'jumlah' => $jumlahs[$key],
				];
			}
		}
		$this->pencatatan_model->changeDetailPencatatan($id_pencatatan, $data);
	}
}
