<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model', 'admin_model');
	}

	public function index()
	{
		if ($this->session->userdata('login')) {
			$this->load->view('layout/header');
			$this->load->view('home');
			$this->load->view('layout/footer');
		} else {
			$this->load->view('login');
		}
	}

	public function proses()
	{
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == TRUE) {
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);

			$login =  $this->admin_model->login($username, $password);
			if ($login->num_rows() != 1) {
				$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Username atau Password Salah']);
				redirect(base_url());
			} else {
				$row = $login->row();
				$data_session = [
					'id_admin' => $row->id_admin,
					'username' => $row->username,
					'telepon' => $row->telepon,
					'jenis_kelamin' => $row->jenis_kelamin,
					'nama' => $row->nama,
					'tanggal_lahir' => $row->tanggal_lahir,
					'login' => true
				];

				$this->session->set_userdata($data_session);
				$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil Login!']);

				redirect(base_url());
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
