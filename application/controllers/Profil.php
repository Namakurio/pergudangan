<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model', 'admin_model');

		if (!$this->session->userdata('login')) {
			redirect(base_url());
		}
	}

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('profil');
		$this->load->view('layout/footer');
	}

	public function update($type)
	{
		if ($type == 'password') {
			$this->form_validation->set_rules('password-now', 'Password Saat Ini', 'required');
			$this->form_validation->set_rules('password-new', 'Password Baru', 'required');
			$this->form_validation->set_rules('reply-password-new', 'Ulang Password Baru', 'required');

			if ($this->form_validation->run() == TRUE) {
				$id_admin = $this->session->userdata('id_admin');
				$password_now = $this->input->post('password-now', TRUE);
				$password_new = $this->input->post('password-new', TRUE);
				$reply_password_new = $this->input->post('reply-password-new', TRUE);

				$check_password =  $this->admin_model->check_password($id_admin, $password_now);
				if ($check_password->num_rows() != 1) {
					$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Password Saat ini salah']);
				} else {
					if ($password_new == $reply_password_new) {
						$this->admin_model->change_password($id_admin, $password_new);
						$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil mengubah password']);
					} else {
						$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Password yang Anda masukkan tidak sama']);
					}
				}
			}
			return redirect(base_url());
		} else if ($type == 'akun') {
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('telepon', 'Telepon', 'required');
			$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
			$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');

			if ($this->form_validation->run() == TRUE) {
				$id_admin = $this->session->userdata('id_admin');

				$update_profil =  $this->admin_model->update_profil($id_admin);
				if ($update_profil) {
					$data_session = [
						'username' => $_POST['username'],
						'nama' => $_POST['nama'],
						'telepon' => $_POST['telepon'],
						'jenis_kelamin' => $_POST['jenis_kelamin'],
						'tanggal_lahir' => $_POST['tanggal_lahir'],
					];

					$this->session->set_userdata($data_session);
					$this->session->set_flashdata('alert', ['type' => 'success', 'message' => 'Berhasil memperbarui profil']);
				} else {
					$this->session->set_flashdata('alert', ['type' => 'warning', 'message' => 'Gagal memperbarui profil']);
				}
			}

			return redirect(base_url('profil'));
		}
	}
}
