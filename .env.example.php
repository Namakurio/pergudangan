<?php

/** Define Host */
define('HOST', 'https://pergudangan.mahasiswa.local/');

/** Define App */
define('APP_NAME', 'Pergudangan');
define('APP_ENV', 'development');

/** Define Database */
define('DATABASE_DRIVER', 'mysqli');
define('DATABASE_HOST', 'localhost');
define('DATABASE_PORT', '3306');
define('DATABASE_USER', 'root');
define('DATABASE_PASS', '');
define('DATABASE_NAME', 'mahasiswa_pergudangan');

/** Define Company */
define('COMPANY_NAME', 'PT Kinderyoy');
define('COMPANY_ADDRESS', 'Jl. Kyai Saleh');
